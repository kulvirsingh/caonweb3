<?php include_once('admin-header.php') ;?>
<?php
$query = "select e.* from ".$obj->getTable("var_menu_manager")." e   where 1=1    order by id desc" ;
$result   = $obj->my_query($query);
$totalRow = mysql_num_rows($result);


?>

     <!-- main area -->
      <div class="main-content">
        <div class="row">
          <div class="col-md-12">
           

            <div class="panel">
              <div class="panel-heading"><h4>List of Menus</h4></div>
                <div class="toolbar">
                            <ol class="breadcrumb breadcrumb-transparent nm">
                            
                              <button style="float:right" type="submit" onclick="redirect_action('menu-add.php','add','<?php echo $v["id"];?>') ;" class="btn btn-success">Add New Menu</button>
                                
                            </ol>
                        </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-condensed responsive mb0" data-sortable>
                    <thead>
                      <tr>      
                                        <th>Menu Title</th>
                                         <th>Parent Link </th>
                                         <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
        $i=0;
        while($v = mysql_fetch_array($result))
        {
            
            $class = ($i%2==0)?'even':'odd';
            ?>


       <tr>
                                         <td><?php echo $v["menu_title"];?></td>
                                          <td><?php echo $v["parent_id"]==0?'Yes':'No';?></td>
                                     
                                        <td> <div class="btn-group">


            <?php  if($v['status'] == 1 ) { ?>
          <a  class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top"  data-original-title="De-Activate"   title=""  href="javascript:void(0);" onclick="redirect_action('menu-activate.php','deactivate','<?php echo $v["id"];?>') ;"><img src="images/active.gif" border="0"></a>
             <?php }else { ?>
             <a class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top"  data-original-title="Activate"   title="" href="javascript:void(0);"  onclick="redirect_action('menu-activate.php','activate','<?php echo $v["id"];?>') ;"><img src="images/deactive.gif" border="0"></a>
             <?php } ?>         
 <a class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top"  data-original-title="Edit"   title="" href="javascript:void(0);"  onclick="redirect_action('menu-add.php','edit','<?php echo $v["id"];?>') ;"><img src="images/icon_edit.gif" border="0"></a>
 <a class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top"  data-original-title="Delete"   title="" href="javascript:void(0);" onclick="redirect_action('menu-delete.php','delete','<?php echo $v["id"];?>') ;"><img src="images/remove.png" border="0"></a>
                                  </div> </td></tr>

 
         
        <?php
        $i++;
        
        }

        ?>
                                  
                                </tbody>
                               
                            </table>
                       </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->





<form name="redFrm" id="redFrm" method="POST">
<input type="hidden" name="user_action" id="user_action"  />
<input type="hidden" name="editKey"  id="editKey"/>
</form>
<script>




function redirect_action(frmAction, action, id)
{
    document.getElementById("user_action").value=action;
    document.getElementById("editKey").value=id;
    document.getElementById("redFrm").action=frmAction;
    
    
    
    if(action=='delete')
    {
        ans = confirm("Are you sure. Do you want to delete this menu?");
        if(ans==true)
            document.redFrm.submit();
        
    }
    else
    {
        document.redFrm.submit();
    }
}



function view_detail(record_id)
{
    show_popup('view_user_details.php?id='+record_id+'&btn=off');
}

</script>




<?php include_once('admin-footer.php')?>