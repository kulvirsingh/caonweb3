<?php include_once('admin-header.php') ;?>

<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_budget")," and id=$edit_key");  
}
?>

<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>

<script src="../jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#cat').on('change',function(){
        var catID = $(this).val();
		
	if(catID){
            $.ajax({
                type:'POST',
                url:'ajaxs.php',
                data:'iid='+catID,
                success:function(html){
					$('#service').html(html);
                    $('#city').html('<option value="">Select subcategory first</option>'); 
                }
           }); 
        }
		
		
		else{
            $('#service').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select subcategory first</option>'); 
        }
    });
    
  
});
</script>

        <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit budget</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">

                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'budget-edit-submit.php':'budget-add-submit.php';?>" method="post" data-parsley-validate>
                                          
                            <div class="panel-body">
							
								<div class="form-group">
								<label class="col-sm-2 control-label">City<span class="text-danger">*</span></label>
		
                <div class="col-sm-6" >
                  <?php  
						 $query = "select e.* from ".$obj->getTable("var_city")." e   where 1=1    order by id desc" ;
                         $result   = $obj->my_query($query);
                         $totalRow = mysql_num_rows($result); 

                    ?>
                                     <select name="city_name" class="form-control" id="cat" >
									 <option value="">select Your city</option>
						<?php				 $i=0;
                           while($v = mysql_fetch_array($result)) {
							   
							   $pid=$v["city_name"].','.$v["id"]; ?>
							   
                                    
				<option value="<?php echo $pid ; ?>" <?php if($v['city_name']==$res['city_name']) echo 'selected="selected"'; ?> ><?php echo $v["city_name"];?></option>
		                <?php } ?>
                                        </select>


                                    </div>
                                </div>
								
								
							
							<div class="form-group">
								<label class="col-sm-2 control-label">service<span class="text-danger">*</span></label>
		
                <div class="col-sm-6" >
                 
           <select name="service" id="service" class="form-control" >
        <option value="">Select Service</option>
          </select>


                                    </div>
                                </div>
								
                                <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                           <label class="col-sm-2 control-label">Budget <span class="text-danger">*</span></label>
                                   
                          <div class="col-sm-6">     
                          <select name="budget" class="form-control">
						   <option value="">Select Budget</option>
                          <option value="0-5000">0-5000</option>
                          <option value="5000-10000">5000-10000</option>
                          <option value="10000-15000">15000-20000</option>
                          <option value="20000-25000">20000-25000</option>
                          </select>
								 
                                </div>

                               

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save budget</button>
                            </div>
                        </form>
                   


              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->


 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#cm').addClass('open');

});
</script>


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>