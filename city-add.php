<?php include_once('admin-header.php') ;?>
<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_city")," and id=$edit_key");  
}
?>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>

        <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit City</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">

                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'city-edit-submit.php':'city-add-submit.php';?>" method="post" data-parsley-validate>
                                          
                            <div class="panel-body">
                                <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">City Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="city_name" class="form-control" value="<?php echo ($_POST["city_name"]!="")? $_POST["city_name"]:$res["city_name"];?>" required>
                                    </div>
                                    
                                </div>

                               

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save city</button>
                            </div>
                        </form>
                   


              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->


 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#cm').addClass('open');

});
</script>


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>