<?php
include("session.php");
include_once("../codelibrary-old/inc/variables.php");
include_once("../codelibrary-old/inc/functions.php");
$obj= new database_class(); 
$func_obj = new common_function() ;
$ad= new ad_class();
$obj_paging = new paging;
$adminid=$_SESSION["sess_admin_id"];
$admin_rs = $obj->getAnyTableWhereData($obj->getTable("var_admin_login_table")," and id=".$_SESSION["sess_admin_id"]); 
/*$modules=$ad->get_module();
foreach($modules as $mod)
{
define($mod['name']."_MODULE",$mod['id']);
}*/
?>
<!doctype html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8">
<title>Find Your Professional : Admin Area</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="styles/climacons-font.css">
<link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
<link rel="stylesheet" href="vendor/summernote/dist/summernote.css">
<link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
<link rel="stylesheet" href="styles/roboto.css">
<link rel="stylesheet" href="styles/font-awesome.css">
<link rel="stylesheet" href="styles/panel.css">
<link rel="stylesheet" href="styles/feather.css">
<link rel="stylesheet" href="styles/animate.css">
<link rel="stylesheet" href="styles/urban.css">
<link rel="stylesheet" href="styles/custom.css">
<link rel="stylesheet" href="styles/urban.skins.css">
<link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="scripts/jquery.dataTables.min.css">
<!-- endbuild -->
</head>
<body>
<div class="app layout-fixed-header">
<!-- sidebar panel -->
<div class="sidebar-panel offscreen-left">
<div class="brand">
<!-- logo -->
<div class="brand-logo">
<a href="https://www.caonweb.com/admin/welcome.php"><img src="../images/website-logo.png" height="40" alt="" style="width: 135px;"></a>
</div>
<!-- /logo -->
<!-- toggle small sidebar menu -->
<a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
 <span></span>
 <span></span>
<span></span>
 <span></span>
 </a>
 <!-- /toggle small sidebar menu -->
</div>
 <!-- main navigation -->
<nav role="navigation">

 <ul class="nav">
 <!-- dashboard -->
 <li id="wc">
<a href="welcome.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a>
</li>
 <!-- /dashboard -->
 <!-- charts -->
 <li id="ad">
 <a href="admin-manage.php"><i class="fa fa-user-secret"></i><span>Admin Users</span></a>
</li>
        <!-- /charts -->
<!-- ui -->
<!--<li id="cm">
<a href="city-manage.php"><i class="fa fa-road"></i><span>City Manager</span></a>
</li>-->
<li id="sm">
<a href="service-manage.php"><i class="fa fa-briefcase"></i><span>Service Manager</span></a>
</li>
 <!--<li id="cm">
           <a href="budget-manage.php">
            <i class="fa fa-toggle-on"></i>
            <span>Budget Manager</span>
          </a>
          </li>
		  -->
<!--<li id="cam"><a href="cms-manage.php"><i class="fa fa-user"></i><span>CA Manager</span></a>
</li>-->

<!--<li id="tm">
<a href="new-professional-registration.php"><i class="fa fa-users" aria-hidden="true"></i><span>New Professional <br>Registration</span></a>
</li>-->

<li id="car">
<a href="ca-registration.php"><i class="fa fa-sign-in"></i><span>New CA Registration</span></a>
</li>
<li id="caa">
<a href="ca-approved.php"><i class="fa fa-thumbs-up"></i><span>CA Approved</span></a>
</li>
<li id="ps">
<a href="payment-status.php"><i class="fa fa-credit-card"></i><span>Payment Status</span></a>
</li>
<li id="nq">
<a href="new-quotation.php"><i class="fa fa-quote-left"></i><span>Popular Discussion</span></a>
</li>  
<li id="qe">
<a href="new-query.php"><i class="fa fa-question-circle"></i><span>New Query</span></a>
</li>
<li id="ds">
<a href="check-feedback.php"><i class="fa fa-check"></i><span>Professional Query</span></a>
</li>

<li id="sa">
<a href="survey-manage.php"><i class="fa fa-check"></i><span>Survey Report</span></a>
</li>
<!--<li id="bm">
<a href="event-manage.php">
<i class="fa fa-eject"></i>
<span>Event Manager</span>
</a>
</li>
-->
<!-- /ready pages -->
<!-- menu levels -->
<!-- <li id="sl">
<a href="slider-manage.php">
<i class="fa fa-level-down"></i>
<span>Home Sliders</span>
</a></li> -->
<!-- menu levels -->
<!-- ready pages 
<li id="gm">
<a href="gallery-manage.php">
<i class="fa fa-cc-discover"></i>
 <span>Gallery Manager</span>
 </a> </li>-->
 <!-- /ready pages -->
 <!-- ready pages -->
 <!-- ready pages -->
 <!--  <li id="mm">
 <a href="menu-manage.php">
  <i class="fa fa-send"></i>
  <span>Menu Manager</span>
 </a>  </li>  -->
<!-- /ready pages -->
<!-- menu levels -->
<!--<li id="ts">
<a href="testimonial-manage.php">
<i class="fa fa-level-down"></i>
<span>Testimonials</span>
</a>
</li> -->
<!-- <li id="wd">
 <a href="widget-manage.php">
  <i class="fa fa-level-down"></i>
 <span>Frontend Widgets</span>
</a>
</li> --><!--<li id="bms">
 <a href="brand-manage.php">
<i class="fa fa-level-down"></i>
<span>Clients</span>
</a></li>
--><!-- menu levels -->
 <!-- menu levels -->
<!-- <li id="cl">
 <a href="resume-manage.php">
  <i class="fa fa-level-down"></i>
  <span>Career Enquiry</span>
</a>
 </li>-->
 <!-- menu levels -->
 <!-- documentation -->
 <li>
 <a href="logout.php"><i class="fa fa-sign-out"></i><span>Logout</span></a>
 </li> <!-- /documentation -->
 </ul>
</nav>
 <!-- /main navigation -->
</div>
<!-- /sidebar panel -->
 <!-- content panel -->
  <div class="main-panel">
 <!-- top header -->
<header class="header navbar">
 <div class="brand visible-xs">
 <!-- toggle offscreen menu -->
<div class="toggle-offscreen">
 <a href="#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
 <span></span>
 <span></span>
 <span></span>
  </a>
</div>
 <!-- /toggle offscreen menu -->
  <!-- logo -->
<div class="brand-logo">
<img src="images/logo-dark.png" height="15" alt="">
 </div>
 <!-- /logo -->
 <!-- toggle chat sidebar small screen-->
<div class="toggle-chat">
 <a href="javascript:;" class="hamburger-icon v2 visible-xs" data-toggle="layout-chat-open">
 <span></span>
 <span></span>
<span></span>
 </a>
 </div> <!-- /toggle chat sidebar small screen-->
</div>
<ul class="nav navbar-nav hidden-xs">
 <li>
  <p class="navbar-text">
 <?php if($_POST["msg"]!="") { ?>
<div id="mydiv" class="alert alert-success" style="width:160%;text-align:center;margin-left:auto;text-transform:uppercase;margin-top:10px;padding:5px !important">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<strong><?php echo $_POST["msg"];?></strong> 
 </div>
<?php } ?> 
 </p>
 </li>
</ul>
 <ul class="nav navbar-nav navbar-right hidden-xs">
 <li>
 <a href="javascript:;" data-toggle="dropdown">
 <img src="images/avatar.png" class="header-avatar img-circle ml10" alt="user" title="user">
<span class="pull-left"><?= $admin_rs['first_name']." ".$admin_rs['last_name']?> </span>
 </a>
 <ul class="dropdown-menu">
  <li>
   <a href="../" target="_blank">View Site</a>
 </li>
 <li>
  <a href="logout.php">Logout</a>
   </li>
   </ul>
  </li>
 </ul>
 </header>



      <!-- /top header -->