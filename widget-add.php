<?php include_once('admin-header.php') ;?>
<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_widget")," and id=$edit_key");  

}

  $positions = $obj->getAnyTableAllData($obj->getTable("var_widget_position")," ");  

?>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>

       <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Widgets</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'widget-edit-submit.php':'widget-add-submit.php';?>" method="post" data-parsley-validate>
                                      
                            <div class="panel-body">
                                <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Widget Title<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="title" class="form-control" value="<?php echo ($_POST["title"]!="")? $_POST["title"]:$res["title"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>


 <div class="form-group">
                                    <label class="col-sm-2 control-label">Position<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">

    <select name="position_id" class="form-control" id="position_id" >
     
   <?php foreach ($positions as $cat):?>
    <option value="<?php echo $cat['id']?>"  <?php if($res['position_id']==$cat['id'])echo 'selected=selected'?>><?php echo $cat['name']?></option>
    <?php endforeach?>
    </select>


                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Description<span class="text-danger"></span></label>
                                    <div class="col-sm-6">
                                        <textarea   id="description"  class="form-control" name="description"><?php echo $res['description']?></textarea>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>


 
                                

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save widget</button>
                            </div>
                        </form>
                   

              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#wd').addClass('open');

});
</script>
<?php include_once('admin-footer.php')?>