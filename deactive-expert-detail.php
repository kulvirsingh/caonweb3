<?php include_once('admin-header.php') ;?>
<?php
	$edit_key = $_POST["editKey"] ;
	if($edit_key) {
	    $res = $obj->getAnyTableWhereData($obj->getTable("var_expert_login_table")," and id=$edit_key");  
	  
	}
	?>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>
<script src="jquery.min.js"></script>
<script src="jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function()
	{  
	$('#country').on('change',function()
	{
	var countryID =$(this).val();
	
	alert(countryID);
	
	if(countryID)
	
	{
	$.ajax
	({
	type:'POST',
	url:'ajaxcity.php',
	data:'country_id='+countryID,
	success:function(html)
	{
		
	//alert(html);
	
	$('#state').html(html);
	$('#city').html('<option value="">Select State First</option>');
	}
	
	
	});
	}else
	{
	$('#state').html('<option value="">Select Country First</option>');
	$('#city').html('<option value="">Select State First</option>');
	
	}
	 })
	
	$('#state').on('change',function()
		{
	var stateID =$(this).val();
	//alert(stateID);
	
	if(stateID)
	
	{
	$.ajax
	({
	type:'POST',
	url:'ajaxcity.php',
	data:'state_id='+stateID,
	success:function(html)
	{
		//alert(html);
	$('#city').html(html);
	}
	
	
	});
	}else
	{
	$('#city').html('<option value="">Select State First</option>');
	
	}
	
		})	
	
	});
	
</script>
<!-- main area -->

<div class="main-content">




<!--page title end-->
					<div class="container-fluid">
						<!-- state start-->
						<div class="row">
							<div class=" col-sm-12 ">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
<ol class="breadcrumb breadcrumb-transparent nm">
<button style="float:left" type="submit" onclick="redirect_action('deactive-expert-manage.php','add','') ;" class="btn btn-success">Back Deactive List</button>
</ol>	
										
										
										</div>
									</div>
									<div class="card-body">
										<ul class="nav nav-tabs mb-4" role="tablist">
											<li class="nav-item">
												<a class="nav-link active  " data-toggle="tab" href="#tab_1">Basic Details</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#tab_3">Address</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#tab_4">Expertise</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#tab_5">Qualification</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#tab_6">Feedback</a>
											</li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active  " id="tab_1" role="tabpanel">
												
												<div class="card-body">
													
						
											<form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'deactive-expert-detail-submit.php':'';?>" method="post" data-parsley-validate>	
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
															<div class="col-sm-4">
																<input type="text" class="form-control form-control-sm" name="name" value="<?php echo ($_POST["name"]!="")? $_POST["name"]:$res["name"];?>"  placeholder="First Name">
																<input type="hidden" name="id" value="<?php echo $res['id']; ?>">
															</div>
															<div class="col-sm-4">
																<input type="text" class="form-control form-control-sm" name="last_name" <?php echo ($_POST["last_name"]!="")? $_POST["last_name"]:$res["last_name"];?>  placeholder="Last Name">
															</div>
														</div>
														
													
													 <div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Gender</label>
															<div class="col-sm-4">
																<label class="custom-control custom-radio">
																<input id="radio1" name="gender" type="radio" value="Male"<?php if ($res['gender'] == 'Male') echo ' checked="checked"'; ?>  class="custom-control-input">
																<span class="custom-control-indicator"></span> <span class="custom-control-description">Male</span> </label>
															</div>
															<div class="col-sm-4">
																<label class="custom-control custom-radio">
																<input id="radio1" name="gender" type="radio" value="Female"<?php if ($res['gender'] == 'Female') echo ' checked="checked"'; ?>  class="custom-control-input">
																<span class="custom-control-indicator"></span> <span class="custom-control-description">Female</span> </label>
															</div>
														</div>
														
														
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
															<div class="col-sm-8">
																<input type="email" class="form-control form-control-sm" name="email" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>"  placeholder="Email">
															</div>
														</div>
														
														
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Mobile Number</label>
															<div class="col-sm-4">
																<input type="number" class="form-control form-control-sm" name="phone" value="<?php echo ($_POST["phone"]!="")? $_POST["phone"]:$res["phone"];?>" id="colFormLabelSm" placeholder="Mobile Number">
															</div>
														</div>
														<div class="form-group row">
															<label  class="col-sm-2 col-form-label col-form-label-sm">Headline</label>
															<div class="col-sm-8">
																<input type="text" class="form-control form-control-sm" value="<?php echo ($_POST["headline"]!="")? $_POST["headline"]:$res["headline"];?>" name="headline" id="colFormLabelSm" placeholder="Headline">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-2"	>Summary</label>
															<div class="col-sm-8">
																<textarea class="form-control" name="summary" id="exampleFormControlTextarea1" rows="4"><?php echo $res['summary']; ?></textarea>
															</div>
														</div>
														<div class="form-group row">
															<label  class="col-sm-2 col-form-label col-form-label-sm">Upload Profile Picure</label>
															<div class="col-sm-8">
													<input type="file" name="profile_photo" class="form-control form-control-sm" value="<?php echo ($_POST["profile_photo"]!="")? $_POST["profile_photo"]:$res["profile_photo"];?>" name="headline" id="colFormLabelSm" placeholder="Headline">
															</div>
															</div>
															
															
						     <div class="form-group row">
							 <label class="col-sm-2"></label>
															<div class="col-sm-8">
															
							<?php if($edit_key!="" && $res["profile_photo"]!=""){?>
                              <input type="hidden" name="front_cart_edit"  value="<?php echo ($_POST["profile_photo"]!="")? $_POST["profile_photo"]:$res["profile_photo"];?>"/>
                              <img src="https://www.caonweb.com/expert-login/images/tmp/<?php echo $res["profile_photo"];?>" width="80" height="80"/>
                             <?php } ?> 
							 
							 </div>
							 </div>
														
														<div class="form-group row">
															<div class="col-sm-2"	></div>
															<div class="col-sm-8">
																 <a class="next btn btn-default btn-design" href="#">Next</a>
															</div>
														</div>
												</div>
										</div>
											
											
							<div class="tab-pane" id="tab_3" role="tabpanel">
												<div class="card-body">
													
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Address</label>
															<div class="col-sm-8">
																<input type="text" name="address" class="form-control form-control-sm" value="<?php echo ($_POST["address"]!="")? $_POST["address"]:$res["address"];?>" placeholder="Address">
															</div>
														</div>
									<?php
                                    $query = "select e.* from ".$obj->getTable("var_country")." e   where 1=1   order by id desc" ;
                                    $result   = $obj->my_query($query);
                                    $totalRow = mysql_num_rows($result);
                                     ?>				
									<div class="form-group row">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Country</label>
															
									<div class="col-sm-8">
									<select name="country" class="form-control form-control-sm" id="country">
									   <option value="Hong Kong,14">Select Country </option>
									 <?php
                                    while($v1 = mysql_fetch_array($result))
                                    { $pid=$v1["country"].','.$v1["id"]; 
                                     ?>
									  <option value="<?php echo  $pid ; ?> "  <?php if($v1['country']==$res['country']) echo 'selected="selected"'; ?>><?php echo $v1["country"];?></option>
									<?php } ?>	
									</select>
									</div>
									</div>
														
										<div class="form-group row">
											<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">State</label>
											<div class="col-sm-8">
												 <select class="form-control form-control-sm" name="state" id="state" >
									              <?php $sid=$res["state"].','.$res["id"]; ?>
									              <option value="<?php echo $sid ; ?>"><?php echo $res['state'] ; ?></option>
									              </select>
												</div>
								    	</div>
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">City</label>
															<div class="col-sm-8">
																<input type="text" class="form-control form-control-sm" name="city" value="<?php echo $res['city'] ; ?>" placeholder="city">
															</div>
														</div>
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Pincode</label>
															<div class="col-sm-8">
																<input type="text" class="form-control form-control-sm" name="pincode" value="201301"  placeholder="Pincode">
															</div>
														</div>
														<div class="form-group row">
															<div class="col-sm-2"	></div>
															<div class="col-sm-8">
																 <a class="next2 btn btn-default btn-design" href="#">Next</a>
															</div>
														</div>
													</div>
							                    </div>
<?php
$query = "select e.* from ".$obj->getTable("var_services_name")." e   where 1=1   order by id desc" ;
$result   = $obj->my_query($query);
$totalRow = mysql_num_rows($result);
?>					
	
											
					
					<div class="tab-pane" id="tab_4" role="tabpanel">
					
					<div class="card-body"> 
 <?php
        $i=0;
        while($v = mysql_fetch_array($result))
        { $class = ($i%2==0)?'even':'odd';
	   $service = $v["service"];
 ?>									
 							
					<label class="custom-control custom-checkbox">
<?php   $res['service'] ;
        $hby = explode(",",$res['service']); ?>					
					<input type="checkbox" name="service[]" value="<?php echo $v["service"];?>"<?php if(in_array("$service",$hby)){?> checked="checked"<?php } ?>  class="custom-control-input" >
					<span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $v["service"];?></span></label>
<?php } ?>						
					
							
					</div>
												
					<div class="form-group row">
						<div class="col-sm-2"	></div>
						<div class="col-sm-8">
						 <a class="next3 btn btn-default btn-design" href="#">Next</a>
						</div>
					</div>
					</div>
											<div class="tab-pane" id="tab_5" role="tabpanel">
												<div class="card-body">
													
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Educational Qualifications </label>
															<div class="col-sm-5">
																
																<select class="form-control form-control-sm" name="eduction">
                                                                <option value="Chartered Accountant" <?php if('Chartered Accountant'==$res['eduction']) echo 'selected="selected"'; ?>>Chartered Accountant</option>
                                                                <option value="Certified Public Accountants" <?php if('Certified Public Accountants'==$res['eduction']) echo 'selected="selected"'; ?>>Certified Public Accountants </option>
                                                                <option value="Company Secretary" <?php if('Company Secretary'==$res['eduction']) echo 'selected="selected"'; ?>>Company Secretary</option>
                                                                </select>
																
															</div>
														</div>
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Membership Number(if applicable)</label>
															<div class="col-sm-5">
																<input type="text" class="form-control form-control-sm" value="<?php echo $res['membership_number'] ; ?>" name="membership_number" placeholder="Membership Number(if applicable)">
															</div>
														</div>
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Member Since </label>
															<div class="col-sm-5">
															    <input type="date" class="form-control form-control-sm" value="<?php echo $res['member_since'] ; ?>" name="member_since" placeholder="Member Since">
																
															</div>
														</div>
														<div class="form-group row">
															<label for="colFormLabelSm" class="col-sm-5 col-form-label col-form-label-sm">Other Highest Degree (if applicable)</label>
															<div class="col-sm-5">
																<input type="text" class="form-control form-control-sm" value="<?php echo $res['highest_degree'] ; ?>" name="highest_degree" placeholder="Highest Degree">
															</div>
														</div>
														
														<div class="form-group row">
															<div class="col-sm-2"	></div>
															<div class="col-sm-8">
																 <a class="next4 btn btn-default btn-design" href="#">Next</a>
															</div>
														</div>
														
												</div>
											</div>
											<div class="tab-pane" id="tab_6" role="tabpanel">
												<div class="card-body">
												
														<div class="form-group row">
															<label class="col-sm-2"	>Feedback</label>
															<div class="col-sm-8">
																<textarea class="form-control" name="feedback" id="exampleFormControlTextarea1" rows="4"  ><?php echo $res['feedback']; ?></textarea>
															</div>
														</div>
														
														<div class="form-group row">
															<div class="col-sm-2"	></div>
															<div class="col-sm-8">
																  <button type="submit" class="btn btn-success save-details">Save</button>
															</div>
														</div>
														
													</form>
													
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- state end-->
					</div>

</div>
</div>
<!-- /main area -->
</div>



<form name="redFrm" id="redFrm" method="POST">
<input type="hidden" name="user_action" id="user_action"  />
<input type="hidden" name="editKey"  id="editKey"/>
</form>

<script>

function redirect_action(frmAction, action, id)
{
    document.getElementById("user_action").value=action;
    document.getElementById("editKey").value=id;
    document.getElementById("redFrm").action=frmAction;
    
    
    
    if(action=='delete')
    {
        ans = confirm("Are you sure. Do you want to delete this user?");
        if(ans==true)
            document.redFrm.submit();
        
    }
    else
    {
        document.redFrm.submit();
    }
}



function view_detail(record_id)
{
    show_popup('view_user_details.php?id='+record_id+'&btn=off');
}

</script>


	
<script>
      $(document).ready(function(){
          $('.next').click(function(){
              $('.nav-tabs a[href="#tab_3"]').tab('show');
          });
      });
	  
	  $(document).ready(function(){
          $('.next2').click(function(){
              $('.nav-tabs a[href="#tab_4"]').tab('show');
          });
      });
	  
	   $(document).ready(function(){
          $('.next3').click(function(){
              $('.nav-tabs a[href="#tab_5"]').tab('show');
          });
      });
	  
	   $(document).ready(function(){
          $('.next4').click(function(){
              $('.nav-tabs a[href="#tab_6"]').tab('show');
          });
      });
	
</script>






<!-- /content panel -->
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://www.caonweb.com/expert-login/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$( document ).ready(function() {
	  
	    $('#tm').addClass('open');
	
	});
</script>
<!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>