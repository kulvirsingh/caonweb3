<?php include_once('admin-header.php') ;?>
<?php

$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_slider")," and id=$edit_key");  
}
?>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>

        <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Sliders</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'slider-edit-submit.php':'slider-add-submit.php';?>" method="post" data-parsley-validate>
                            <div class="panel-heading">
                                <h3 class="panel-title">Please Fill the details</h3>
                            </div>               
                            <div class="panel-body">
                                <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Slider title<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="page_title" class="form-control" value="<?php echo ($_POST["page_title"]!="")? $_POST["page_title"]:$res["page_title"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>


                                   <div class="form-group">

                                    <label class="col-sm-2 control-label">Slider Subtitle<span class="text-danger"></span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="title2" class="form-control" value="<?php echo ($_POST["title2"]!="")? $_POST["title2"]:$res["title2"];?>">
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>

                                 <div class="form-group">

                                    <label class="col-sm-2 control-label">Click URL<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="link" class="form-control" value="<?php echo ($_POST["link"]!="")? $_POST["link"]:$res["link"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>



                              


                                 <div class="form-group">
                                        <label class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-6">
                                            <input type="file" name="thumb_image" id="thumb_image" value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
    
                                           
                                        </div>

                                         <div class="col-sm-4">
                                        <?php if($edit_key!="" && $res["thumb_image"]!=""){?>
   <input type="hidden" name="front_cart_edit"  value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
   <img src="<?php echo UPLOADS_PATH.$res["thumb_image"];?>" width="80" height="80"/>
  
   <?php }?> 
                                    </div>
                                    </div>

                              


 
                                

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save Page</button>
                            </div>
                        </form>
                   

              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->



       

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#sl').addClass('open');

});
</script>


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>