<?php include_once('admin-header.php') ;?>

<?php
$result_arr=array();
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_user_login_table")," and id=$edit_key");
      }
?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Admin User</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
               
 <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'admin-edit-submit.php':'admin-add-submit.php';?>" method="post" data-parsley-validate>
                                        
                            <div class="panel-body">
                                <div class="form-group">
								<input type="hidden" name="id" id="id" value="<?= $edit_key?>" />
                                    <label class="col-sm-2 control-label">First Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
									 <input type="text" name="first_name" class="form-control" value="<?php echo ($_POST["first_name"]!="")? $_POST["first_name"]:$res["first_name"];?>" required>
                                    </div>
                                  
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Last Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="last_name" class="form-control" value="<?php echo ($_POST["last_name"]!="")? $_POST["last_name"]:$res["last_name"];?>"  required>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>


                              

                                  
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" class="form-control" data-parsley-type="email" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
								  <div class="form-group">
                                    <label class="col-sm-2 control-label">Phone<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
        <input type="text" name="phone" class="form-control" data-parsley-type="phone" value="<?php echo ($_POST["phone"]!="")? $_POST["phone"]:$res["phone"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
								
								
								
								
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="password" name="password" class="form-control" data-parsley-length="[5, 8]" value="<?php echo ($_POST["password"]!="")? $_POST["password"]:$res["password"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
				<div class="form-group">
                   
					 <label class="col-sm-2 control-label"><b>Service Expertise</b><span class="text-danger">*</span></label>
                      <div class="col-sm-9">
                        
						<div>



<?php
$query = "select e.* from ".$obj->getTable("var_service")." e   where 1=1   order by id desc" ;
$result   = $obj->my_query($query);
$totalRow = mysql_num_rows($result);
?>	

	
 <?php
        $i=0;
        while($v = mysql_fetch_array($result))
        { $class = ($i%2==0)?'even':'odd';
	    $service = $v["service_name"];
 ?>							
		
					
<?php   $res['service_name'] ;
        $hby = explode(",",$res['service_name']); ?>
		
		<div class="col-sm-6">
		<input type="checkbox" id="service" name="service[]" value="<?php echo $v["service_name"];?>"<?php if(in_array("$service",$hby)){?> checked="checked"<?php } ?>  class="custom-control-input" >
		
		<span class="custom-control-indicator"></span>
		<span class="custom-control-description"><?php echo $v["service_name"];?></span>
		
		</div>
<?php } ?>	

</div>
						
						
                      </div>
                    </div>
								

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save User</button>
                            </div>
                        </form>





              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#ad').addClass('open');

});
</script>
    <?php include_once('admin-footer.php') ;?>