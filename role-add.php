<?php include_once('admin-header.php') ;?>
<?php



$edit_key = $_POST["editKey"] ;
$arr=array();
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_role")," and id=$edit_key"); 
    $arr=unserialize(html_entity_decode(base64_decode($res['permission'])));
   
}
 $category = $obj->getAnyTableAllData($obj->getTable("var_module")," order by id");


?>
<script type="text/javascript">



function checkAll(bx) {
    
  var cbs = document.getElementsByClassName('chk');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
    
  }
}



</script>
            <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Roles</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
               
                        <form name="newsFrm" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'role-edit-submit.php':'role-add-submit.php';?>" method="post" data-parsley-validate>
                                         
                            <div class="panel-body">
                               

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Role<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
       <input type="text" name="role_name" id="role"   class="form-control"  value="<?php echo ($_POST["role_name"]!="")? $_POST["role_name"]:$res["role"];?>" required/>
                                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Permission<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
 <table border="0" width="550px" cellpadding="10px" cellspacing="5px;">
    <tr><td colspan="4"><input type="checkbox" name="all" id="all" onclick="checkAll(this)"  style="position:relative;top:2px;"/> &nbsp; Select All &nbsp; <div style="color:#F00" id="per_fld"></td></tr>
         <?php
         ?>                       
                                    <?php foreach ($category as $cat): ?>
                                    <tr>
                                      <td><?php echo strtoupper(str_replace('_',' ',$cat['name'])) ?>   MODULE</td>
                                      <td><input class="chk" type="checkbox" value="1" id="<?php echo $cat['name'] ?>[]"   name="<?php echo $cat['name'] ?>[]"  <?php if(in_array(1,$arr[$cat['id']-1])) echo 'checked="checked"';?>/>
                                        View </td>
                                        
                                         <td><input class="chk" type="checkbox" value="2"  id="<?php echo $cat['name'] ?>[]" name="<?php echo $cat['name'] ?>[]"  <?php if(in_array(2,$arr[$cat['id']-1])) echo 'checked="checked"';?>/>Add </td>
                                         
                                         
                                      <td><input class="chk" type="checkbox" value="3"  id="<?php echo $cat['name'] ?>[]" name="<?php echo $cat['name'] ?>[]"  <?php if(in_array(3,$arr[$cat['id']-1])) echo 'checked="checked"';?>/>
                                        Edit </td>
                                      <td><input class="chk" type="checkbox" value="4"  id="<?php echo $cat['name'] ?>[]"  name="<?php echo $cat['name'] ?>[]" <?php if(in_array(4,$arr[$cat['id']-1])) echo 'checked="checked"';?>/>
                                        Delete </td>
                                    </tr>
                                    <?php endforeach ?>
                                  </table>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>



                              





                                  
                               


                            
                                


                                

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save Role</button>
                            </div>
                        </form>
                 


              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#ad').addClass('open');

});
</script>
    <?php include_once('admin-footer.php') ;?>