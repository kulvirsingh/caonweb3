<?php include_once('admin-header.php') ;?>
<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_ca_manager")," and id=$edit_key");  
}
?>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>
<script src="../jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#country').on('change',function(){
        var countryID = $(this).val();
		if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'id='+countryID,
                success:function(html){
					$('#state').html(html);
                    $('#city').html('<option value="">Select service first</option>'); 
                }
           }); 
        }
		
		
		else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select service first</option>'); 
        }
    });
    
    $('#state').on('change',function(){
        var stateID = $(this).val();
		
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'state_id='+stateID,
                success:function(html){
					
					
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select service first</option>'); 
        }
    });
});
</script>

        <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Pages</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">

                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'cms-edit-submit.php':'cms-add-submit.php';?>" method="post" data-parsley-validate>
                                          
                            <div class="panel-body">
                                <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="name" class="form-control" value="<?php echo ($_POST["name"]!="")? $_POST["name"]:$res["name"];?>" required>
                                    </div>
                                    
                                </div>
								
								 <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">email<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" class="form-control" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>" required>
                                    </div>
                                    
                                </div>
								
								
								 <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Phone<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="phone" class="form-control" value="<?php echo ($_POST["phone"]!="")? $_POST["phone"]:$res["phone"];?>" required>
                                    </div>
                                    
                                </div>
								
								<div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Company<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="company" class="form-control" value="<?php echo ($_POST["company"]!="")? $_POST["company"]:$res["company"];?>" required>
                                    </div>
                                    
                                </div>
								
									<div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Pin Code<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="pin" class="form-control" value="<?php echo ($_POST["pin"]!="")? $_POST["pin"]:$res["pin"];?>" required>
                                    </div>
                                    
                                </div>
								
								
		<div class="form-group">
				<label class="col-sm-2 control-label">City<span class="text-danger">*</span></label>
		 <?php  
			$query = "select e.* from ".$obj->getTable("var_city")." e   where 1=1    order by id desc" ;
            $result   = $obj->my_query($query);
            $totalRow = mysql_num_rows($result); 
         ?>
       
	   <div class="col-sm-6" >
                  <select name="city_name" id="country" style="height: 40px;"  >
					 <option value="">select Your city</option>
			<?php	
				 while($v = mysql_fetch_array($result)) { 
				  $pid=$v["city_name"].','.$v["id"];
			?>
			
	<option value="<?php echo $pid ; ?>" <?php if($v['city_name']==$res['city_name']) echo 'selected="selected"'; ?> ><?php echo $v["city_name"];?></option>
		     <?php } ?>
                                 
						  </select>
    </div>
         </div>

		       <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Area<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="area" class="form-control" value="<?php echo ($_POST["area"]!="")? $_POST["area"]:$res["area"];?>" required>
                                    </div>
                                    
                                </div>	
								
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">location<span class="text-danger"></span></label>
                                    <div class="col-sm-10">
                                        <textarea   class="summernote"   id="description" name="location"><?php echo $res['location']?></textarea>
                                    </div>
                                   
                                
								</div>
								
							  

                      <div class="form-group">
                                   <!-- <label class="col-sm-2 control-label">Show on Home Page<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                      <span class="radio">  
                                                <input type="radio" name="show_home" value="0" <?php if($res['show_home']==0)echo 'checked=checked'?> >  
                                                <label for="show_home">&nbsp;&nbsp; No </label>   
                                            </span>

                                             <span class="radio">  
                                                <input type="radio" name="show_home" value="1" <?php if($res['show_home']==1)echo 'checked=checked'?> >  
                                                <label for="show_home">&nbsp;&nbsp; Yes </label>   
                                            </span>
                                    </div>
                                    <div class="col-sm-4">
                                   This is shown in home page learning solution section.
                                    </div> -->
                                </div>


                                 <div class="form-group">
                                        <label class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-6">
                                            <input type="file" name="thumb_image" id="thumb_image" value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
    
                                           
                                        </div>

                                         <div class="col-sm-4">
                                        <?php if($edit_key!="" && $res["thumb_image"]!=""){?>
   <input type="hidden" name="front_cart_edit"  value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
   <img src="<?php echo UPLOADS_PATH.$res["thumb_image"];?>" width="80" height="80"/>
  
   <?php }?> 
                                    </div>
                                    </div>

                                 <div class="form-group">
								<label class="col-sm-2 control-label">Service<span class="text-danger">*</span></label>
		
                <div class="col-sm-6" >
                   <select name="service" id="state" style="height: 40px;" >
                   <option value="">Select Service</option>
                   </select>

                                    </div>
                                </div>



                     <div class="form-group">
								<label class="col-sm-2 control-label">Budget<span class="text-danger">*</span></label>
		
                <div class="col-sm-6" >
                 <select name="budget" id="city" style="height: 40px;">
                   <option value="">Select Budget</option>
                     </select>


                                    </div>
                                </div>




 
                                

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save Page</button>
                            </div>
                        </form>
                   


              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->


 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#cm').addClass('open');

});
</script>


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>