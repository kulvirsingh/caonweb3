<?php include_once('admin-header.php') ;?>
<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_menu_manager")," and id=$edit_key");  
}
//'1'=>"Category",
$arr=array('2'=>"CMS Page",'3'=>"Static Links");

 $cat = $obj->getAnyTableAllData($obj->getTable("var_menu_manager")," and parent_id=0");  
?>

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>
 <script type="text/javascript">


function showData(param, pageId)
{
  parameters="";
  if(pageId!='')
    {
      parameters= parameters+"&pagenum="+pageId ;
    }
     
 /*//alert(parameters);


       callAjax("tt", "menu.php", {
    
      params:parameters+"&rand="+Math.random(),
    
      meth:"post",
    
      async:false,
    
      startfunc:"proccess_begin();",
    
      endfunc:"ends();",
    
      errorfunc:"" }
    
      );  
*/if(pageId!='3')
{
 
$.ajax({
          type: "POST",
          cache: false,
          url: "menu.php",
          data: 'pagenum='+ pageId ,
          success: function(data) {
          
            $("#tt").html(data);
          
          }
        });
$('#menu_url').prop('disabled', true);
 $('#menu_url2').prop('disabled', false);

}

else
{
  $('#menu_url2').prop('disabled', true);
  $('#menu_url').prop('disabled', false);
}
      
      /*});*/
}

function ends()
{
  $("#cat_list").fadeIn("fast");
}

function proccess_begin()
{
   document.getElementById("cat_list").innerHTML = "<br><br><img src='images/loadingAnimation.gif'><br >Loading ..." ;
}

<?php if ($edit_key != "" ) { ?>

showData('list','<?php echo $res["menu_type"]; ?>') ;
  <?php } ?>
</script>
         <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Menus</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'menu-edit-submit.php':'menu-add-submit.php';?>" method="post" data-parsley-validate>
                                      
                            <div class="panel-body">
                                <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>
  <input type="hidden" name="editKey" id="editKey" value="<?php echo $edit_key ; ?>"  />
                                    <label class="col-sm-2 control-label">Menu Title<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="menu_title" class="form-control" value="<?php echo ($_POST["menu_title"]!="")? $_POST["menu_title"]:$res["menu_title"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>

                               <div class="form-group">
                                  <label class="col-sm-2 control-label">Parent Menu</label>
                                    <div class="col-sm-6">
                                       
<select  name="parent_id" id="parent_id" class="form-control">
<option value="0">Select Parent Menu</option>
<?php foreach($cat as $val):?>
<option value="<?php echo $val['id'] ;?>" <?php if($val['id']==$res['parent_id']){echo 'selected="selected"' ;}?> ><?php echo $val['menu_title'] ;?></option>

<?php endforeach ?>
           </select>
                                         </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>



                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Menu Type<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                       
<select  name="menu_type" id="menu_type" class="form-control" onchange="showData('list',this.value);" required>
<option value="">Select Menu Type</option>
<?php foreach($arr as $key=>$val):?>
<option value="<?php echo $key ;?>" <?php if($key==$res['menu_type']){echo 'selected="selected"' ;}?> ><?php echo $val ;?></option>

<?php endforeach ?>
           </select>
                                         </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>

                              

                               <div class="form-group">
                         
                                    <label class="col-sm-2 control-label">Select<span class="text-danger">*</span></label>
                                    <div class="col-sm-6" id="tt">

                                             <select class="form-control" id="menu_url2" name="menu_url2"></select>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>





                                
                                   

 
                                

                                     <div class="form-group">
                         
                                    <label class="col-sm-2 control-label">Menu URL<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" name="menu_url" id="menu_url"  value="<?php echo $res['menu_url'] ?>" />
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>





                                    <div class="form-group">
                         
                                    <label class="col-sm-2 control-label">Display order<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" name="display_order" id="display_order" maxlength="100" value="<?php echo $res['display_order'] ?>" />
                                      </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>




                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save Menu</button>
                            </div>
                        </form>
                      </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>