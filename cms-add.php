<?php include_once('admin-header.php') ;?>
<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_ca_regestation")," and id=$edit_key");  
}
?>
<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>
<script src="../jquery.min.js"></script>


        <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Pages</h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">

                        <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'cms-edit-submit.php':'cms-add-submit.php';?>" method="post" data-parsley-validate>
                                          
                            <div class="panel-body">
							
							
		<div class="form-group">
		 <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>
				
				<label class="col-sm-2 control-label">City<span class="text-danger">*</span></label>
		 <?php  
			$query = "select e.* from ".$obj->getTable("var_city")." e   where 1=1    order by id desc" ;
            $result   = $obj->my_query($query);
            $totalRow = mysql_num_rows($result); 
         ?>
       
	   <div class="col-sm-6" >
                  <select name="city_name" id="country"  class="form-control" >
					 <option value="">select Your city</option>
			<?php	
				 while($v = mysql_fetch_array($result)) { 
				  $pid=$v["city_name"].','.$v["id"];
			?>
			
	<option value="<?php echo $pid ; ?>" <?php if($v['city_name']==$res['city_name']) echo 'selected="selected"'; ?> ><?php echo $v["city_name"];?></option>
		     <?php } ?>
                                 
						  </select>
    </div>
         </div>
							
							
				 <div class="form-group">
                   
					  <label class="col-sm-2 control-label">Service<span class="text-danger">*</span></label>
                      <div class="col-sm-9">
                        <div class="tick-box">
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Company Incorporation Modification">
                            <span class="check-text">Company Incorporation Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="LLP Incorporation Modification">
                            <span class="check-text">LLP Incorporation Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Service Tax Registration Modification">
                            <span class="check-text">Service Tax Registration Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="VAT Registration Modification">
                            <span class="check-text">VAT Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="GST Registration Modification">
                            <span class="check-text">GST Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="IEC Registration Modification">
                            <span class="check-text">IEC Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trademark Application Objection">
                            <span class="check-text">Trademark Application / Objection</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Income Tax Return">
                            <span class="check-text">Income Tax Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Company Annual Filing">
                            <span class="check-text">Company Annual Filing</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="LLP Annual Filing">
                            <span class="check-text">LLP Annual Filing</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Service Tax Return">
                            <span class="check-text">Service Tax Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="VAT Return">
                            <span class="check-text">VAT Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Secretarial Compliances">
                            <span class="check-text">Secretarial Compliances</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="CA Certification">
                            <span class="check-text">CA Certification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="TDS Return">
                            <span class="check-text">TDS Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trust Formation">
                            <span class="check-text">Trust Formation</span></div>
                        </div>
                      </div>
                    </div>			
						
                                <div class="form-group">
                          

                                    <label class="col-sm-2 control-label">Consultant Fee<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select  name="budget" class="form-control">
                          <option>Select Your Budget</option>
                          <option value= "2000-5000" >2000-5000</option>
                          <option value= "5000-10000" >5000-10000</option>
                          <option value= "10000-20000" >10000-20000</option>
                          <option value= "20000-25000" >20000-25000</option>
                          <option value= "25000-30000" >25000-30000</option>
                          <option value= "30000-35000" >30000-35000</option>
                        </select>
                                    </div>
                                    
                                </div>

								
                                <div class="form-group">
                          

                                    <label class="col-sm-2 control-label">Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="name" class="form-control" value="<?php echo ($_POST["name"]!="")? $_POST["name"]:$res["name"];?>" required>
                                    </div>
                                    
                                </div>
								
								 <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">email<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" class="form-control" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>" required>
                                    </div>
                                    
                                </div>
								
								
								 <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Phone<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="phone" class="form-control" value="<?php echo ($_POST["phone"]!="")? $_POST["phone"]:$res["phone"];?>" required>
                                    </div>
                                    
                                </div>
								
								<div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Company<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="company" class="form-control" value="<?php echo ($_POST["company"]!="")? $_POST["company"]:$res["company"];?>" required>
                                    </div>
                                    
                                </div>
								
								<div class="form-group">
                                 <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Pin Code<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="pincode" class="form-control" value="<?php echo ($_POST["pincode"]!="")? $_POST["pincode"]:$res["pincode"];?>" required>
                                    </div>
                                    
                                </div>
								
								<div class="form-group">
                                 <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Address<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea rows="4" cols="71" name="location"><?php echo $res['location']?></textarea>
                                    </div>
                                    
                                </div>
		

		       <div class="form-group">
                           <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>

                                    <label class="col-sm-2 control-label">Area<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="area" class="form-control" value="<?php echo ($_POST["area"]!="")? $_POST["area"]:$res["area"];?>" required>
                                    </div>
                                    
                                </div>	
								
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">About Ca<span class="text-danger"></span></label>
                                    <div class="col-sm-10">
                                        <textarea   class="summernote" id="description" name="about_ca"><?php echo $res['about_ca']?></textarea>
                                    </div>
                                   
                                
								</div>
								
							  

                     


                                 <div class="form-group">
                                        <label class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-6">
                                    <input type="file" name="thumb_image" id="thumb_image" value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
    
                                           
                                        </div>

                                         <div class="col-sm-4">
                                        <?php if($edit_key!="" && $res["thumb_image"]!=""){?>
                             <input type="hidden" name="front_cart_edit"  value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
                              <img src="<?php echo UPLOADS_PATH.$res["thumb_image"];?>" width="80" height="80"/>
  
   <?php }?> 
                                    </div>
                                    </div>

                                 



                     




 
                                

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save Page</button>
                            </div>
                        </form>
                   


              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->


 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#cm').addClass('open');

});
</script>


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>